package com.carrefour.services;

import com.carrefour.entities.ProductEntity;
import com.carrefour.mappers.ProductMapper;
import com.carrefour.models.Product;
import com.carrefour.repositories.ProductRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class ProductService {

    @Autowired
    private final ProductRepository productRepository;

    @Autowired
    private final ProductMapper productMapper;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll().stream()
                .map(productMapper::entityToModel)
                .collect(Collectors.toList());
    }

    public Product getProductBySku(String sku) {
        return productMapper.entityToModel(productRepository.findBySku(sku));
    }

    public Product updateProduct(String sku, Product product) {
        ProductEntity existingProduct = productRepository.findBySku(sku);
        if (existingProduct == null) return null;
        existingProduct = productMapper.modelToEntity(product);
        return productMapper.entityToModel(productRepository.save(existingProduct));
    }
}
