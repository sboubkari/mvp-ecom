package com.carrefour.mappers;

import com.carrefour.entities.ProductEntity;
import com.carrefour.models.Product;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface ProductMapper {
    Product entityToModel(ProductEntity productEntity);
    ProductEntity modelToEntity(Product product);
}
