package com.carrefour.models;


import lombok.*;

import java.io.Serializable;

@Data
public class Product implements Serializable {

    protected String sku;

    protected String name;

    protected Double price;

    protected Integer quantity;

}
