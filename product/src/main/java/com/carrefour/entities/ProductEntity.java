package com.carrefour.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class ProductEntity {

    @Id
    protected String sku;

    protected String name;

    protected Double price;

    protected Integer quantity;

}
