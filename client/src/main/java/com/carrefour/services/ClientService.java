package com.carrefour.services;

import com.carrefour.mappers.ClientMapper;
import com.carrefour.models.Client;
import com.carrefour.repositories.ClientRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class ClientService {

    @Autowired
    private final ClientRepository clientRepository;

    @Autowired
    private final ClientMapper clientMapper;

    public ClientService(ClientRepository clientRepository, ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    public List<Client> getAllClients() {
        return clientRepository.findAll().stream()
                .map(clientMapper::entityToModel)
                .collect(Collectors.toList());
    }

    public Client getClientByEmail(String email) {
        return clientMapper.entityToModel(clientRepository.findByEmail(email));
    }

}
