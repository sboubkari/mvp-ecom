package com.carrefour.models;


import lombok.*;

import java.io.Serializable;

@Data
public class Client implements Serializable {

    protected Long clientId;

    protected String firstName;

    protected String lastName;

    protected String email;

}
