package com.carrefour.mappers;

import com.carrefour.entities.ClientEntity;
import com.carrefour.models.Client;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface ClientMapper {
    Client entityToModel(ClientEntity clientEntity);
    ClientEntity modelToEntity(Client client);
}
