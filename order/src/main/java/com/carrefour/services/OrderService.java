package com.carrefour.services;

import com.carrefour.controllers.OrderHttpResponse;
import com.carrefour.entities.OrderEntity;
import com.carrefour.entities.ProductOrderEntity;
import com.carrefour.mappers.OrderMapper;
import com.carrefour.models.Cart;
import com.carrefour.models.Client;
import com.carrefour.models.Order;
import com.carrefour.repositories.OrderRepository;
import com.carrefour.repositories.ProductOrderRepository;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private final RestTemplate restTemplate;

    private final String clientApiUrl = "http://localhost:9105/clients/";

    private final String cartApiUrl = "http://localhost:9104/carts/";

    @Autowired
    private final OrderRepository orderRepository;

    @Autowired
    private ProductOrderRepository productOrderRepository;

    @Autowired
    private OrderMapper orderMapper;

    public OrderService(RestTemplate restTemplate, OrderRepository orderRepository, ProductOrderRepository productOrderRepository, OrderMapper orderMapper) {
        this.restTemplate = restTemplate;
        this.orderRepository = orderRepository;
        this.productOrderRepository = productOrderRepository;
        this.orderMapper = orderMapper;
    }

    public Order createOrder(String email) {
        Client client = restTemplate.getForObject(clientApiUrl + email,
                Client.class);
        OrderEntity savedOrder = new OrderEntity();


        if (client != null) {
            Long clientId = client.getClientId();
            List<Cart> carts = Arrays.asList(restTemplate.getForObject(cartApiUrl + clientId, Cart[].class));

            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setClientId(clientId);
            orderEntity.setStatus("CONFIRMED");
            orderEntity.setDateHour(LocalDateTime.now());
            savedOrder = orderRepository.save(orderEntity);

            saveProductOrderEntities(carts, savedOrder);
        }

        return orderMapper.entityToModel(savedOrder);
    }

    private void saveProductOrderEntities(List<Cart> carts, OrderEntity order) {
        for (Cart cart : carts) {
            ProductOrderEntity productOrderEntity = new ProductOrderEntity();
            productOrderEntity.setOrder(order);
            productOrderEntity.setSku(cart.getSku());
            productOrderEntity.setQuantityOrdered(cart.getQuantityOrdered());

            productOrderRepository.save(productOrderEntity);

        }
    }

    public OrderHttpResponse cancelOrder(Long id) {
        OrderEntity updatedOrder = new OrderEntity();
        OrderEntity orderEntity = orderRepository.findByOrderId(id);
        LocalDateTime dateTime = orderEntity.getDateHour();
        LocalDateTime now = LocalDateTime.now();
        // Calcluer la durée entre la date de l'ordre et et le now
        Duration duration = Duration.between(dateTime, now);
        if (duration.toHours() < 1) {
            // la durée est inférieur à 1h , on peut annuler la commande
            orderEntity.setStatus("CANCELED");
            updatedOrder = orderRepository.save(orderEntity);
            Order order = orderMapper.entityToModel(updatedOrder);
            OrderHttpResponse httpResponse = orderMapper.modelToHttpResponse(order);
            httpResponse.setHttpStatus(200);
        }
        OrderHttpResponse response = new OrderHttpResponse(403);
        response.setError("La commande ne peux étre annulé, Merci");
        response.setOrderId(id);
        response.setStatus(orderEntity.getStatus());
        return response;
    }
}
