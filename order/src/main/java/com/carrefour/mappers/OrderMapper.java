package com.carrefour.mappers;

import com.carrefour.controllers.OrderHttpResponse;
import com.carrefour.entities.OrderEntity;
import com.carrefour.models.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    Order entityToModel(OrderEntity orderEntity);

    OrderHttpResponse modelToHttpResponse(Order order);
}
