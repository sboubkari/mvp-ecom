package com.carrefour.controllers;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderHttpResponse {
    int httpStatus;
    private Long orderId;
    private String status;
    private String error;

    public OrderHttpResponse(int httpStatus){
        this.httpStatus = httpStatus;
    }
}
