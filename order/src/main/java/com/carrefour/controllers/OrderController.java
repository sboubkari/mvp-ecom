package com.carrefour.controllers;

import com.carrefour.models.Order;
import com.carrefour.services.OrderService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Transactional("transactionManager")
@Data
@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/{email}")
    public Order createOrder(@PathVariable String email) {
        return orderService.createOrder(email);
    }

    @PutMapping("{idOrder}")
    public OrderHttpResponse cancelOrder(@PathVariable Long idOrder) {
        return orderService.cancelOrder(idOrder);
    }
}
