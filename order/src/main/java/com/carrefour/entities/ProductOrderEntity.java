package com.carrefour.entities;


import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "product_order")
@Data
@ToString
public class ProductOrderEntity {

    @Id
    @Column(name = "product_order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long prodyctOrderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private OrderEntity order;

    private String sku;

    @Column(name = "quantity_ordered")
    private Integer quantityOrdered;


}
