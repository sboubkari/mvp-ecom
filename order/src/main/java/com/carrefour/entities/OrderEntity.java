package com.carrefour.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
@ToString
public class OrderEntity {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    @Column(name = "client_id")
    private Long clientId;

    private String status;

    @Column(name = "date_hour")
    private LocalDateTime dateHour;

    @OneToMany(mappedBy = "order")
    private List<ProductOrderEntity> products;
}
