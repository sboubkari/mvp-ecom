package com.carrefour.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class Cart implements Serializable {


    private Long cartId;

    private Long clientId;

    private String sku;

    private Integer quantityOrdered;
}
