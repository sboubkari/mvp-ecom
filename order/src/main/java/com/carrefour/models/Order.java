package com.carrefour.models;

import com.carrefour.entities.ProductOrderEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class Order implements Serializable {


    private Long orderId;

    private Long clientId;

    private String status;

    private LocalDateTime dateHour;

}
