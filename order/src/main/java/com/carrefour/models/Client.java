package com.carrefour.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client implements Serializable {

    protected Long clientId;

    protected String firstName;

    protected String lastName;

    protected String email;

}
