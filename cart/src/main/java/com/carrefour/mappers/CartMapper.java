package com.carrefour.mappers;

import com.carrefour.entities.CartEntity;
import com.carrefour.models.Cart;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartMapper {
    Cart entityToModel(CartEntity cartEntity);
    CartEntity modelToEntity(Cart cart);
}
