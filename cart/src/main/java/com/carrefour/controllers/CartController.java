package com.carrefour.controllers;

import com.carrefour.models.Cart;
import com.carrefour.models.ProductCartInput;
import com.carrefour.services.CartService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Transactional("transactionManager")
@Data
@RestController
@RequestMapping("/carts")
public class CartController {

    @Autowired
    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }


    @PostMapping("/")
    public Cart addProduct(@RequestBody ProductCartInput productCartInput) {
        return cartService.addProduct(productCartInput);
    }

    @DeleteMapping("/{email}/{sku}")
    public void deleteProduct(@PathVariable String email ,@PathVariable String sku) {
        cartService.deleteProduct(email, sku);
    }

    @GetMapping("/{idClient}")
    public List<Cart> getCartsByIdClient(@PathVariable Long idClient) {
        return cartService.getCartsByIdClient(idClient);
    }
}
