package com.carrefour.repositories;

import com.carrefour.entities.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<CartEntity, Long> {
    CartEntity findByClientIdAndSku(Long clientId, String sku);
    void deleteByClientIdAndSku(Long clientId, String sku);
    List<CartEntity> findByClientId(Long clientId);

}
