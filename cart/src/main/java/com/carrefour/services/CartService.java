package com.carrefour.services;

import com.carrefour.entities.CartEntity;
import com.carrefour.mappers.CartMapper;
import com.carrefour.models.Cart;
import com.carrefour.models.Client;
import com.carrefour.models.Product;
import com.carrefour.models.ProductCartInput;
import com.carrefour.repositories.CartRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Data
@Transactional("transactionManager")
public class CartService {

    @Autowired
    private final CartRepository cartRepository;
    @Autowired
    private final RestTemplate restTemplate;

    @Autowired
    private final CartMapper cartMapper;

    private final String productApiUrl = "http://localhost:9103/products/";

    private final String clientApiUrl = "http://localhost:9105/clients/";


    public CartService(CartRepository cartRepository,
                       RestTemplate restTemplate,
                       CartMapper cartMapper) {
        this.cartRepository = cartRepository;
        this.restTemplate = restTemplate;
        this.cartMapper = cartMapper;
    }


    public Cart addProduct(ProductCartInput productCartInput) {

        Product product = restTemplate.getForObject(productApiUrl + productCartInput.getSku(),
                Product.class);
        Client client = restTemplate.getForObject(clientApiUrl + productCartInput.getEmail(),
                Client.class);
        Cart cartSaved = null;

        if (product != null && client != null) {
            Long clientId = client.getClientId();
            // chercher s'il y a ce produit dans le panier du client
            CartEntity existingProduct = cartRepository.findByClientIdAndSku(clientId, productCartInput.getSku());
            if (existingProduct != null) {
                Integer oldQuantity = existingProduct.getQuantityOrdered();
                Integer newQuantity = oldQuantity + productCartInput.getQuantity();
                existingProduct.setQuantityOrdered(newQuantity);
                cartSaved = cartMapper.entityToModel(cartRepository.save(existingProduct));
            } else {
                CartEntity newCartProduct = new CartEntity();
                newCartProduct.setClientId(clientId);
                newCartProduct.setSku(productCartInput.getSku());
                newCartProduct.setQuantityOrdered(productCartInput.getQuantity());
                cartSaved = cartMapper.entityToModel(cartRepository.save(newCartProduct));
            }
            // mis à jour du stock
            Integer newQuanityInStock = product.getQuantity() - productCartInput.getQuantity();
            product.setQuantity(newQuanityInStock);
            restTemplate.put(productApiUrl + productCartInput.getSku(), product);

        }

        return cartSaved;
    }

    public void deleteProduct(String email, String sku) {
        Client client = restTemplate.getForObject(clientApiUrl + email,
                Client.class);

        if (client != null) {
            Long clientId = client.getClientId();
            cartRepository.deleteByClientIdAndSku(clientId, sku);
        }
    }

    public List<Cart> getCartsByIdClient(Long idClient) {
        return cartRepository.findByClientId(idClient)
                .stream()
                .map(cartMapper::entityToModel)
                .collect(Collectors.toList());
    }

    public String getProductApiUrl() {
        return productApiUrl;
    }

    public String getClientApiUrl() {
        return clientApiUrl;
    }
}
