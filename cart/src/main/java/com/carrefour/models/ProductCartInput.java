package com.carrefour.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductCartInput {

    private String email;

    private String sku;

    private Integer quantity;
}
