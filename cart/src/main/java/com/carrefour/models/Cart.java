package com.carrefour.models;

import lombok.Data;

@Data
public class Cart {


    private Long cartId;

    private Long clientId;

    private String sku;

    private Integer quantityOrdered;
}
