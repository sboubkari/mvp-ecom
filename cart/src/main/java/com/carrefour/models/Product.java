package com.carrefour.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
public class Product implements Serializable {

    private String sku;

    private String name;

    private Double price;

    private Integer quantity;
}

