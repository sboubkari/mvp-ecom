package com.carrefour.services;

import com.carrefour.entities.CartEntity;
import com.carrefour.mappers.CartMapper;
import com.carrefour.mappers.CartMapperImpl;
import com.carrefour.models.Client;
import com.carrefour.models.Product;
import com.carrefour.models.ProductCartInput;
import com.carrefour.repositories.CartRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;


@ExtendWith(MockitoExtension.class)
public class CartServiceTest {

    @Mock
    private CartRepository cartRepository;

    @Mock
    private RestTemplate restTemplate;

    private CartMapper cartMapper;

    private CartService cartService;

    @BeforeEach
    void setUp() {
        cartMapper = new CartMapperImpl();
        cartService = new CartService(cartRepository, restTemplate, cartMapper);
    }

    @Test
    void testAddProductToCart_when_product_dont_exist() {

        Mockito.when(restTemplate.getForObject("http://localhost:9103/products/carrefour-lait-1L" ,
                        Product.class))
                .thenReturn(new Product("carrefour-lait-1L", "lait 1l",2.5, 6));
        Mockito.when(restTemplate.getForObject("http://localhost:9105/clients/s@gmail.com",
                        Client.class))
                .thenReturn(new Client(Long.valueOf(1), "Soufiane","BOUBKARI", "s@gmail.com"));


        // When
        cartService.addProduct(new ProductCartInput("s@gmail.com", "carrefour-lait-1L", 2));

        // Assert
        Mockito.verify(cartRepository).save(new CartEntity(null, 1L, "carrefour-lait-1L", 2));

    }

    @Test
    void testAddProductToCart_when_product_exist() {

        Mockito.when(restTemplate.getForObject("http://localhost:9103/products/carrefour-lait-1L" ,
                        Product.class))
                .thenReturn(new Product("carrefour-lait-1L", "lait 1l",2.5, 6));
        Mockito.when(restTemplate.getForObject("http://localhost:9105/clients/s@gmail.com",
                        Client.class))
                .thenReturn(new Client(1L, "Soufiane","BOUBKARI", "s@gmail.com"));
        Mockito.when(cartRepository.findByClientIdAndSku(1L, "carrefour-lait-1L"))
                .thenReturn(new CartEntity(1L, 1L, "carrefour-lait-1L", 3));


        // When
        cartService.addProduct(new ProductCartInput("s@gmail.com", "carrefour-lait-1L", 2));

        // Assert
        Mockito.verify(cartRepository).save(new CartEntity(1L, 1L, "carrefour-lait-1L", 5));


    }

    @Test
    void testDeleteProduct_from_cart() {
        Mockito.when(restTemplate.getForObject("http://localhost:9105/clients/s@gmail.com",
                        Client.class))
                .thenReturn(new Client(1L, "Soufiane","BOUBKARI", "s@gmail.com"));

        // When
        cartService.deleteProduct("s@gmail.com", "carrefour-lait-1L");

        // Assert
        Mockito.verify(cartRepository).deleteByClientIdAndSku(1L, "carrefour-lait-1L");
    }
}
