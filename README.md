# Ecommerce MVP

# Architecture

![img_2.png](e-commerce.svg)

Ceci est une architecture microservices/serverless d'un MVP e-commerce
Le projet est consitué de 5 microservices évolutives
##### 1 - products : fournit les informations sur les produits
##### 2 - cart :  gérer le panier d'un client
##### 3 - client: gérer les clients
##### 4 - orders : gérer les commandes de clients.


Chaque microservice est constitudé d'un AWS LAMBDA Function, un endpoint et une base de données.
Un API rest managé par AWS api gateway

à La création/annulation de commande un message est push dans une file de message "SQS" qui rentre dans un pattern de "pub/sub" qui déclenche un service de messagerie
abonné (géré via SNS) capable d'envoyer un email de confirmation/annulation de commande.


## Install application

````bash 
mvn clean & mvn install 
Lancer les classes :
- ProduitApplication
- ClientApplication
- OrderApplication
- CartApplication

Les apis sont disponibles sur les urls suivants:
- http://localhost:9103/products/
- http://localhost:9105/clients/
- http://localhost:9106/orders/
- http://localhost:9104/carts/
````

## Deploy the sample application

````bash 
// TODO
  serverless deploy
````